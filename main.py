import numpy as np
import matplotlib.pyplot as plt
from iq_analysis.model.kid_sweep import KidSweep
import iq_analysis.util.tools as tools


def main():
    tmp = np.load("sptslim_sweep.npy", allow_pickle=True, encoding="bytes")
    sweep_data = tmp.item()
    # noinspection PyUnresolvedReferences
    keys = list(sweep_data.keys())

    kid_list = [
        b"K000",
        b"K001",
        b"K002",
        b"K003",
        b"K004",
        b"K005",
        b"K006",
        b"K007",
        b"K008",
        b"K009",
        b"K010",
        b"K011",
        b"K012",
        b"K013",
    ]

    kid_sweep_dict = {}  # Dictionary to store all the KidSweep instances in

    for kid_index, kid in enumerate(kid_list):
        # noinspection PyUnresolvedReferences
        frequency_array = sweep_data[keys[0]][kid_index] + sweep_data[keys[1]]
        # noinspection PyUnresolvedReferences
        iq_array = sweep_data[kid_list[kid_index]]
        i_array = np.real(iq_array)
        q_array = np.imag(iq_array)

        kid_sweep = KidSweep(
            name=str(kid).replace("b", ""),
            frequency_array=frequency_array,
            original_i_array=i_array,
            original_q_array=q_array,
        )

        # Add kid_sweep object to kid sweep dictionary:
        kid_sweep_dict[kid_sweep.name] = kid_sweep

    # Remove extra set of quotation marks from dictionary keys
    kid_sweep_dict = {key.replace("'", ""): val for key, val in kid_sweep_dict.items()}

    show_calibration_plots = False
    kid_name = "K001"
    if show_calibration_plots:
        kid_sweep_dict[kid_name].plot_original_iq(
            save_plot=False, save_name="", plot_title=kid_name + ": Original IQ plot"
        )
        kid_sweep_dict[kid_name].plot_calibrated_iq(
            save_plot=False,
            save_name="",
            plot_title=kid_name
            + ": Calibrated IQ plot after centering, normalisation and rotation",
        )
        kid_sweep_dict[kid_name].plot_frequency_phase_fit(
            save_plot=False,
            save_name="",
            plot_title=kid_name
            + ": Phase to frequency relation and it's polynomial fit",
        )

    # ************************************************************************************

    # Extract Laser Data:
    tmp = np.genfromtxt(r"spt_slim_1_100-200Ghz_step0.3ghz_it10s.csv", delimiter=",")
    laser_time = tmp[:, 0]
    laser_frequency = tmp[:, 1]

    plateau_indices = tools.find_plateaus(
        laser_time, laser_frequency, threshold=0.004, plot_graph=False
    )
    # Array of python times that correspond to the start and ends of each frequency plateau
    plateau_time_bounds = laser_time[plateau_indices]

    # Extract time stream data:
    tmp = np.load("sptslim_laserscan.npy", allow_pickle=True, encoding="bytes")
    time_stream_data = tmp.item()
    keys = list(time_stream_data.keys())

    python_time = time_stream_data[keys[-2]]
    gpio = time_stream_data[keys[-1]]

    # Get indices in corresponding to time stream data that is within the laser plateau times
    time_stream_good_indices = tools.get_cut_indices(python_time, plateau_time_bounds)

    # remove indices from outside of plateaus:
    python_time = python_time[time_stream_good_indices]
    time_stream_i_array = time_stream_data[b"K000_I"][time_stream_good_indices]
    time_stream_q_array = time_stream_data[b"K000_Q"][time_stream_good_indices]
    time_stream_iq = time_stream_i_array + 1j * time_stream_q_array
    time_stream_frequencies = np.interp(python_time, laser_time, laser_frequency)

    # Determine time stream df using circle fit technique:
    df = kid_sweep_dict[kid_name].interpolate_df(
        time_stream_i_array, time_stream_q_array, frequency_array=time_stream_frequencies
    )
    df = df - np.mean(df)

    # Extract nominal df which used the tangent approximation.
    old_df = time_stream_data[b"K000_df"][time_stream_good_indices]
    old_df = old_df - np.mean(old_df)

    #
    on_off_indices = tools.find_demodulation_indices(modulating_signal=gpio)
    # # optimal_index_delay = tools.find_optimal_index_delay(
    # #     signal_array=old_df,
    # #     time_array=python_time,
    # #     demodulation_indices=on_off_indices,
    # #     index_delay_step=10,
    # #     plot_graph=False
    # # )
    optimal_index_delay = 25

    # ********** Demodulate df data arrays **********
    (
        demodulated_signal,
        demodulated_python_time,
        demodulated_signal_error,
    ) = tools.demodulate_signal_dumb(
        signal_array=df,
        time_array=python_time,
        demodulation_indices=on_off_indices + optimal_index_delay,
        plot_graph=False,
    )

    (
        old_df_demodulated_signal,
        old_df_demodulated_python_time,
        old_df_demodulated_signal_error,
    ) = tools.demodulate_signal_dumb(
        signal_array=old_df,
        time_array=python_time,
        demodulation_indices=on_off_indices + optimal_index_delay,
        plot_graph=False,
    )

    # ********** Plot time stream data **********

    plt.figure(figsize=(8, 6))
    plt.errorbar(
        x=demodulated_signal,
        y=old_df_demodulated_signal,
        xerr=demodulated_signal_error,
        yerr=old_df_demodulated_signal_error,
        linestyle="none",
        marker="o"
    )
    plt.xlabel("Circle Fit df")
    plt.ylabel("Tangent Fit df")
    plt.show()

    show_time_stream_data = True

    python_time = python_time - python_time[0]
    laser_time = laser_time - laser_time[0]
    demodulated_python_time = demodulated_python_time - demodulated_python_time[0]
    old_df_demodulated_python_time = old_df_demodulated_python_time - old_df_demodulated_python_time[0]

    plt.figure()
    plt.plot(python_time, old_df, color="k", label="Tangent df")
    plt.plot(python_time, df, color="b", label="Raw df")
    # plt.plot(demodulated_python_time, demodulated_signal, linestyle="--", marker="o", color="r", label="demodulated df")
    plt.legend()
    plt.show()


    if show_time_stream_data:
        start_index = 0
        stop_index = -1
        x_start = 435.6
        x_stop = 436.0

        fig, axs = plt.subplots(3, 1)
        axs[0].plot(
            python_time[start_index:stop_index],
            df[start_index:stop_index],
        )
        axs[0].set_ylabel("Raw Tangent df")
        axs[0].set_xlim(x_start, x_stop)
        axs[0].grid(True)

        axs[1].plot(
            python_time[start_index:stop_index],
            df[start_index:stop_index],
            label="Raw"
        )
        axs[1].plot(
            demodulated_python_time[start_index:stop_index],
            demodulated_signal[start_index:stop_index],
            linestyle="--",
            marker="o",
            color="r",
            markersize=2,
            label="Demodulated"
        )
        axs[1].set_ylabel("Demodulated Circle df")
        axs[1].set_xlim(x_start, x_stop)
        axs[1].legend()

        axs[2].plot(
            python_time[start_index:stop_index],
            old_df[start_index:stop_index],
            label="Raw"
        )
        axs[2].plot(
            old_df_demodulated_python_time[start_index:stop_index],
            old_df_demodulated_signal[start_index:stop_index],
            linestyle="--",
            marker="o",
            color="r",
            markersize=2,
            label="Demodulated"
        )
        axs[2].set_ylabel("Demodulated Tangent df")
        axs[2].set_xlim(x_start, x_stop)
        axs[2].set_xlabel("Time (s)")
        axs[2].legend()

        fig.tight_layout()
        plt.show()


if __name__ == "__main__":
    main()
