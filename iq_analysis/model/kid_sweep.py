import iq_analysis.util.tools as tools
import numpy as np
import matplotlib.pyplot as plt


class KidSweep:
    """
    Class to represent a KID IQ sweep and store the relevant data. Upon creating an instance with original sweep IQ
    data and frequency data, the IQ loop will be calibrated with a circle fit and a frequency vs phase polynomial will
    be fit to allow for IQ interpolation.
    """

    def __init__(
        self,
        name,
        original_i_array,
        original_q_array,
        frequency_array,
    ):
        """

        :param name: Name/ID for the kid, typically "KNNN"
        :param original_i_array: I array from IQ data.
        :param original_q_array: Q array from IQ data
        :param frequency_array: Corresponding frequency array in Hertz.
        """

        self.name = name
        self.original_i_array = original_i_array
        self.original_q_array = original_q_array
        self.frequency_array = frequency_array

        iq_array = original_i_array + 1j * original_q_array

        resonance_index = tools.find_resonance_index(original_i_array, original_q_array)
        off_resonance_index_array = tools.find_off_resonance_index_array(
            i_array=original_i_array, q_array=original_q_array, threshold=0.9
        )

        cable_delay_amplitude, cable_delay_time = tools.fit_cable_delay(
            frequency_array=frequency_array[off_resonance_index_array],
            complex_data_array=iq_array[off_resonance_index_array],
            amplitude_guess=np.sqrt(
                original_i_array[0] ** 2 + original_q_array[0] ** 2
            ),
            tau_guess=80e-9,
            plot_graph=False,
        )

        # Remove cable delay
        cable_delay = tools.cable_delay(
            frequency_array, cable_delay_amplitude, cable_delay_time
        )
        iq_array = iq_array / cable_delay

        i_array = np.real(iq_array)
        q_array = np.imag(iq_array)

        # Fit circle and find centre and radius:
        x_centre, y_centre, r, residual = tools.circle_fit(i_array, q_array)
        # centre and normalise IQ circle:
        i_array = (i_array - x_centre) / r
        q_array = (q_array - y_centre) / r

        # Find angle of resonance w.r.t. centre of circle:
        resonance_angle = np.angle(
            i_array[resonance_index] + 1j * q_array[resonance_index], deg=True
        )

        # Rotate IQ loop to put resonance at (-1, 0)
        rotated_iq_array = tools.rotate_2d_matrix(
            np.array([i_array, q_array]), resonance_angle
        )
        i_array_rotated = rotated_iq_array[0, :]
        q_array_rotated = rotated_iq_array[1, :]
        phase_array = np.angle(i_array_rotated + 1j * q_array_rotated, deg=True)

        rotated_angle = resonance_angle
        # Adjust rotation slightly to get continues phase vs frequency
        while tools.discontinuity_presence(phase_array, threshold=125):
            rotated_iq_array = tools.rotate_2d_matrix(
                np.array([i_array_rotated, q_array_rotated]), 0.1
            )
            i_array_rotated = rotated_iq_array[0, :]
            q_array_rotated = rotated_iq_array[1, :]
            phase_array = np.angle(i_array_rotated + 1j * q_array_rotated, deg=True)
            rotated_angle += 0.1

        # fit polynomial to map phase angle to frequency
        quarter = int(frequency_array.size / 4)
        polynomial_fit = tools.fit_polynomial(
            phase_array[resonance_index - quarter : resonance_index + quarter],
            frequency_array[resonance_index - quarter : resonance_index + quarter],
            degree=6,
            plot_graph=False,
        )

        self.cable_delay = cable_delay
        self.cable_delay_amplitude = cable_delay_amplitude
        self.cable_delay_time = cable_delay_time
        self.rotation_angle = rotated_angle
        self.iq_radius = r
        self.i_centre = x_centre
        self.q_centre = y_centre
        self.phase_array = phase_array
        self.calibrated_i_array = i_array_rotated
        self.calibrated_q_array = q_array_rotated
        self.phase_freq_polynomial = polynomial_fit
        self.f0 = frequency_array[resonance_index]

    def plot_original_iq(
        self, save_plot: bool, save_name: str, plot_title="Original IQ plot"
    ):
        """
        Function to plot the IQ of the original IQ sweep before calibrating.

        :param save_plot: Boolean to save the figure or not.
        :param save_name: Name to save the figure with.
        :param plot_title: Title to give to the plot.
        :return: None
        """

        plt.figure(figsize=(6, 6))
        plt.plot(
            self.original_q_array,
            self.original_i_array,
            linestyle="--",
            marker="o",
            markersize="4",
        )
        plt.xlabel("Q")
        plt.ylabel("I")
        plt.axis("equal")
        plt.title(plot_title)
        plt.show()
        if save_plot:
            plt.savefig(save_name)
        return None

    def plot_calibrated_iq(
        self,
        save_plot: bool,
        save_name: str,
        plot_title="Calibrated IQ plot after centering, normalisation and rotation",
    ):
        """
        Function to plot the IQ of the Calibrated IQ sweep after calibration (centering, normalisation, rotation)

        :param save_plot: Boolean to save the figure or not.
        :param save_name: Name to save the figure with.
        :param plot_title: Title to give to the plot.
        :return: None
        """

        plt.figure(figsize=(6, 6))
        plt.plot(
            self.calibrated_q_array,
            self.calibrated_i_array,
            linestyle="--",
            marker="o",
            markersize="4",
        )
        plt.xlabel("Q")
        plt.ylabel("I")
        plt.axis("equal")
        plt.title(plot_title)
        plt.show()
        if save_plot:
            plt.savefig(save_name)
        return None

    def plot_frequency_phase_fit(
        self,
        save_plot: bool,
        save_name: str,
        plot_title="Phase to frequency relation and it's polynomial fit",
    ):
        """
        Function to plot the frequency vs phase for the kid and its polynomial fit.

        :param save_plot: Boolean to save the figure or not.
        :param save_name: Name to save the figure with.
        :param plot_title: Title to give to the plot.
        :return: None
        """

        plt.figure(figsize=(8, 6))
        plt.plot(
            self.phase_array,
            self.frequency_array * 1e-9,
            linestyle="none",
            marker="o",
            color="b",
            label="Data",
        )
        plt.plot(
            self.phase_array,
            self.phase_freq_polynomial(self.phase_array) * 1e-9,
            color="r",
            linestyle="-",
            label="Fit with polynomial of degree 6",
        )
        plt.xlabel("Phase (Degrees)")
        plt.ylabel("Frequency (GHz)")
        plt.title(plot_title)
        plt.legend()
        plt.show()
        if save_plot:
            plt.savefig(save_name)
        return None

    def interpolate_df(self, i_array: np.ndarray, q_array: np.ndarray, frequency_array: np.ndarray):
        """
        Function to convert I and Q arrays from the corresponding time stream data into a df by interpolating the
        frequency vs phase of the calibrated sweep IQ.
        :param i_array: time_stream data of I values to be interpolated at.
        :param q_array: time_stream data of Q values to be interpolated at.
        :param frequency_array: Optional. Frequencies of optical load corresponding to each IQ value, used to remove the
        cable delay. If None, the mean of the KID's cable delay is used.
        :return:
        """

        # Remove cable delay
        iq_array = i_array + 1j * q_array
        cable_delay_array = tools.cable_delay(
            frequency_array=frequency_array,
            amplitude=self.cable_delay_amplitude,
            tau=self.cable_delay_time
        )

        # iq_array = iq_array / np.mean(self.cable_delay)
        iq_array = iq_array / cable_delay_array
        i_array = np.real(iq_array)
        q_array = np.imag(iq_array)

        # transform IQ points:
        i_array = (i_array - self.i_centre) / self.iq_radius
        q_array = (q_array - self.q_centre) / self.iq_radius

        # Rotate IQ loop to put resonance at (-1, 0)
        rotated_iq_array = tools.rotate_2d_matrix(
            np.array([i_array, q_array]), self.rotation_angle
        )

        i_array = rotated_iq_array[0, :]
        q_array = rotated_iq_array[1, :]
        time_stream_phase = np.angle(i_array + 1j * q_array, deg=True)
        f = self.phase_freq_polynomial(time_stream_phase)
        df = (self.f0 - f) / f

        return df
