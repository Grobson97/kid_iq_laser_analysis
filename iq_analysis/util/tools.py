from scipy import optimize
from lmfit import Model
from lmfit import Parameters
import matplotlib.pyplot as plt
import numpy as np
from numpy.polynomial import Polynomial


def find_resonance_index(i_array: np.ndarray, q_array: np.ndarray) -> int:
    """
    Function to find the index in the I and Q arrays corresponding to the resonant frequency, e.g. the smallest value
    in the magnitude array.

    :param i_array: real component of the IQ data.
    :param q_array: imaginary component of the IQ data.
    :return:
    """

    magnitude_array = np.abs(i_array + 1j * q_array)

    return np.argmin(magnitude_array)


def find_off_resonance_index_array(
    i_array: np.ndarray, q_array: np.ndarray, threshold: float
) -> np.ndarray:
    """
    Function to find the indices in the I and Q arrays corresponding to the values that are off resonance, set by the
    threshold.
    in the magnitude array.

    :param i_array: real component of the IQ data.
    :param q_array: imaginary component of the IQ data.
    :param threshold: Threshold for off resonance. Must be value between 0 and 1. 0 will include all values, 1 will
    include the largest value.
    :return: Array of indexes where values are above the threshold.
    """

    magnitude_array = np.abs(i_array + 1j * q_array)
    value_range = np.max(magnitude_array) - np.min(magnitude_array)
    normalised_mag_array = magnitude_array / value_range - np.min(
        magnitude_array / value_range
    )
    off_resonance_index_array = np.argwhere(normalised_mag_array > threshold)

    return off_resonance_index_array


def plot_iq(iq_data: np.ndarray, save_plot: bool, plot_title: str, save_name: str):
    """
    :param iq_data: complex data array to plot I and Q: I+iQ.
    :param save_plot: Boolean to save plot.
    :param plot_title: Title of the plot.
    :param save_name: name to save plot with, end with ".png".
    :return:
    """
    plt.figure(figsize=(6, 6))
    plt.plot(
        np.real(iq_data), np.imag(iq_data), linestyle="--", marker="o", markersize="4"
    )
    plt.xlabel("I")
    plt.ylabel("Q")
    plt.axis("equal")
    plt.title(plot_title)
    plt.show()
    if save_plot:
        plt.savefig(save_name)


def cable_delay(
    frequency_array: np.ndarray, amplitude: float, tau: float
) -> np.ndarray:
    """
    Function to calculate the cable delay term for a transfer coefficient.
    :param frequency_array: Array of frequencies
    :param amplitude: Amplitude of cable delay
    :param tau: Cable delay time
    :return:
    """

    return amplitude * np.exp(-2 * np.pi * 1j * frequency_array * tau)


def fit_cable_delay(
    frequency_array: np.array,
    complex_data_array: np.array,
    amplitude_guess: float,
    tau_guess: float,
    plot_graph=True,
):
    """

    :param frequency_array:
    :param complex_data_array: Array of complex data.
    :param amplitude_guess:
    :param tau_guess:
    :param plot_graph: Boolean to plot graph of the fit
    :return:
    """

    data_amplitude = np.abs(complex_data_array)
    data_phase_angle = np.angle(complex_data_array)
    data_exponential = data_amplitude * np.exp(1j * data_phase_angle)

    # define model
    cable_delay_model = Model(cable_delay)

    # define parameters:
    params = Parameters()
    params.add("amplitude", value=amplitude_guess, min=0, vary=True)
    # params.add("amplitude", value=7.2e6, min=0, vary=False)
    params.add("tau", value=tau_guess, vary=True)

    result = cable_delay_model.fit(
        data=data_exponential, params=params, frequency_array=frequency_array
    )
    fit_amplitude = (result.best_values["amplitude"],)
    fit_tau = (result.best_values["tau"],)

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(
            np.real(data_exponential),
            np.imag(data_exponential),
            linestyle="none",
            marker="o",
            fillstyle="none",
            label="Data",
        )
        dummy_frequencies = np.linspace(
            start=np.min(frequency_array) - (5 / fit_tau[0]),
            stop=np.max(frequency_array) + (5 / fit_tau[0]),
            num=1000,
        )
        best_fit_circle = cable_delay(dummy_frequencies, fit_amplitude[0], fit_tau[0])
        best_fit = cable_delay(frequency_array, fit_amplitude[0], fit_tau[0])
        plt.plot(
            np.real(best_fit),
            np.imag(best_fit),
            linestyle="none",
            marker="o",
            fillstyle="none",
            label=f"Best fit: Amplitude = {fit_amplitude[0]: .2E}, "
            + r"$\tau$"
            + f" = {fit_tau[0]: .2E}",
        )
        plt.plot(
            np.real(best_fit_circle), np.imag(best_fit_circle), label=f"Best fit circle"
        )
        plt.xlabel("Q")
        plt.ylabel("I")
        plt.title("Cable delay fit")
        plt.axis("equal")
        plt.legend()
        plt.show()

    return fit_amplitude, fit_tau


def calculate_r(
    x_array: np.ndarray, y_array: np.ndarray, x_centre: float, y_centre: float
):
    """
    calculate the distance of each 2D points from the center (xc, yc)

    :param x_array: numpy array of x data
    :param y_array: numpy array of y data
    :param x_centre: centre of circle x coordinate
    :param y_centre: centre of circle y coordinate
    :return:
    """
    return np.sqrt((x_array - x_centre) ** 2 + (y_array - y_centre) ** 2)


def f(
    centre,
    x_array: np.ndarray,
    y_array: np.ndarray,
):
    """calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc)"""
    ri = calculate_r(*centre, x_array, y_array)
    return ri - ri.mean()


def circle_fit(x_array: np.ndarray, y_array: np.ndarray):
    """
    Fits circle to data using least square method shown in scipy.optimize

    :param x_array: array of x data to fit.
    :param y_array: array of y data to fit
    :return: circle fit parameters
    """
    # centre guess:
    x_mean = np.mean(x_array)
    y_mean = np.mean(y_array)
    centre_guess = x_mean, y_mean
    fit_centre, ier = optimize.leastsq(f, centre_guess, args=(x_array, y_array))
    x_fit_centre, y_fit_centre = fit_centre
    r_array = calculate_r(x_array, y_array, x_fit_centre, y_fit_centre)
    r_mean = r_array.mean()
    residual = np.sum((r_array - r_mean) ** 2)

    print(
        f"circle fit complete. Fit parameters:\nx_centre={x_fit_centre: .1E}\ny_centre={y_fit_centre:.1E}\nR={r_mean:.1E}\nResidual={residual:.1E}"
    )

    return x_fit_centre, y_fit_centre, r_mean, residual


def plot_circle(x_centre: float, y_centre: float, r: float, label: str) -> None:
    """
    Function to plot a circle with given centre coordinates and radius to the current plt.figure.

    :param x_centre: x coordinate of circle centre.
    :param y_centre: y coordinate of circle centre.
    :param r: radius of circle
    :param label: Label to be given to the circle data series.
    :return: none
    """
    circle_theta_array = np.linspace(-np.pi, np.pi, 180)
    circle_x_array = x_centre + r * np.cos(circle_theta_array)
    circle_y_array = y_centre + r * np.sin(circle_theta_array)
    plt.plot(circle_x_array, circle_y_array, label="Circle Fit")

    return None


def rotate_2d_matrix(array: np.ndarray, angle: float) -> np.ndarray:
    """
    Function to rotate a 2d matrix by a specified angle in degrees
    :param array: 2D numpy array to be rotated
    :param angle: angle to rotate by.
    :return: Rotated Matrix.
    """

    rotation_matrix = np.array(
        ((np.cos(angle), -np.sin(angle)), (np.sin(angle), np.cos(angle)))
    )

    return rotation_matrix.dot(array)


def discontinuity_presence(array: np.ndarray, threshold: float) -> bool:
    """
    Function to check if there is a discontinuity in the array by looking for large jumps between values. The threshold
    for which determines how big a gap counts as a discontinuity is set by the threshold.

    :param array: 1D Numpy array to be checked for discontinuities.
    :param threshold: Value determining how big a jump counts as a discontinuity
    :return: boolean, True if there is a discontinuity, false if not.
    """

    gradient_array = np.gradient(array)
    difference = 0
    for count, value in enumerate(gradient_array):
        if count < array.size - 1:  # Checks value isn't last in array.
            difference = abs(gradient_array[count + 1] - value)

        if difference > threshold and count > 0:
            return True

    return False


def fit_polynomial(x_data: np.array, y_data: np.array, degree: int, plot_graph: True):
    """
    Function to fit a polynomial of a specified degree to x and y data. Returns a numpy poly1d object.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    :return poly1d object containing the fit coefficients.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial_fit = np.poly1d(coefficients)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial_fit(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree 6",
        )
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.show()

    return polynomial_fit


def find_demodulation_indices(modulating_signal: np.ndarray) -> np.ndarray:
    """
    Function to return the indices that correspond to the indices bounding each on period and each off period in a
    perfectly a square wave signal (only on off states).

    :param modulating_signal: array of data for the modulating signal.
    :return: Array of indices: np.array([[on_start], [on_stop], [off_start], [off_stop]])
    """

    # When state changes from one to the other gradient has two values which are positive or negative, we only want the
    # first value
    if modulating_signal[0] > 0:
        on_first = True
    if modulating_signal[0] < 0:
        on_first = False

    non_zeros_gradient = np.where(np.gradient(modulating_signal) != 0)[0]

    if on_first:
        on_start_indices = np.hstack(
            (np.array([0]), np.array(non_zeros_gradient[3::4]))
        )
        on_stop_indices = np.array(non_zeros_gradient[::4])
        off_start_indices = np.array(non_zeros_gradient[1::4])
        off_stop_indices = np.array(non_zeros_gradient[2::4])
    if not on_first:
        on_start_indices = np.array(non_zeros_gradient[1::4])
        on_stop_indices = np.array(non_zeros_gradient[2::4])
        off_start_indices = np.hstack(
            (np.array([0]), np.array(non_zeros_gradient[3::4]))
        )
        off_stop_indices = np.array(non_zeros_gradient[::4])

    # Check if all arrays are same length, and shortens to the modal size
    sizes = np.array(
        [
            on_start_indices.size,
            on_stop_indices.size,
            off_start_indices.size,
            off_stop_indices.size,
        ]
    )
    values, counts = np.unique(sizes, return_counts=True)
    mode_size = values[np.argmax(counts)]

    on_start_indices = on_start_indices[:mode_size]
    on_stop_indices = on_stop_indices[:mode_size]
    off_start_indices = off_start_indices[:mode_size]
    off_stop_indices = off_stop_indices[:mode_size]

    return np.array(
        [on_start_indices, on_stop_indices, off_start_indices, off_stop_indices]
    )


def find_optimal_index_delay(
    signal_array: np.array,
    time_array: np.ndarray,
    demodulation_indices: np.ndarray,
    index_delay_step: int,
    plot_graph: bool,
) -> int:
    """
    Function to shift the demodulation indices by a fixed value and determine the optimal one, maximising the mean
    mean demodulated signal.

    :param signal_array: Array to be demodulated
    :param time_array: Corresponding time array for signal time stream.
    :param demodulation_indices: Array of four arrays of the indices corresponding the the start and stop of an on
    period, and the start and stop of an on period  np.array([[on_start], [on_stop], [off_start], [off_stop]]).
    :param index_delay_step: Integer step size to use between index delay values. Higher is faster but less precise.
    :param plot_graph: Boolean to plot the graph showing the mean demodulated signal vs. index delay.
    :return Optimal index delay
    """

    on_off_cycle_points = demodulation_indices[3][1] - demodulation_indices[0][1]
    index_delay_list = np.arange(0, 3 * on_off_cycle_points, index_delay_step)
    mean_demodulated_signal = []

    for count, index_delay in enumerate(index_delay_list):
        (
            demodulated_signal,
            demodulated_python_time,
            demodulated_signal_error,
        ) = demodulate_signal_dumb(
            signal_array=signal_array,
            time_array=time_array,
            demodulation_indices=demodulation_indices + index_delay,
            plot_graph=False,
        )
        mean_demodulated_signal.append(np.mean(demodulated_signal))

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(index_delay_list, mean_demodulated_signal)
        plt.xlabel("Index delay"),
        plt.ylabel("Mean value of demodulated signal")
        plt.show()

    return index_delay_list[np.argmax(np.array(mean_demodulated_signal))]


def demodulate_signal_dumb(
    signal_array: np.array,
    time_array: np.ndarray,
    demodulation_indices: np.ndarray,
    plot_graph: bool,
):
    """
    Function to demodulate a signal given and array of indices indicating the on-off cycles. The demodulated signal
    is the mean value of the signal in each on-off cycle, as is the demodulated time. The demodulated signal error is
    the standard deviation divided by the square root of the number of points in the cycle.

    :param signal_array: Array to be demodulated
    :param time_array: Corresponding time array for signal time stream.
    :param demodulation_indices: Array of four arrays of the indices corresponding the the start and stop of an on
    period, and the start and stop of an on period  np.array([[on_start], [on_stop], [off_start], [off_stop]]).
    :param plot_graph: Boolean to plot the graph showing the demodulation.
    :return:
    """
    demodulated_signal = []
    demodulated_time = []
    demodulated_signal_error = []

    for count in range(demodulation_indices[0].size):
        on_mean = np.mean(
            signal_array[
                demodulation_indices[0][count] : demodulation_indices[1][count]
            ]
        )
        off_mean = np.mean(
            signal_array[
                demodulation_indices[2][count] : demodulation_indices[3][count]
            ]
        )
        number_of_points = (
            demodulation_indices[3][count] - demodulation_indices[0][count]
        )
        on_std = np.std(
            signal_array[
                demodulation_indices[0][count] : demodulation_indices[1][count]
            ]
        )
        off_std = np.std(
            signal_array[
                demodulation_indices[2][count] : demodulation_indices[3][count]
            ]
        )

        demodulated_signal.append(on_mean - off_mean)
        demodulated_time.append(
            np.mean(
                time_array[
                    demodulation_indices[0][count] : demodulation_indices[3][count]
                ]
            )
        )
        demodulated_signal_error = (on_std + off_std) / (2 * np.sqrt(number_of_points))

    demodulated_signal_error = np.array(demodulated_signal_error)
    demodulated_signal = np.array(demodulated_signal)
    demodulated_time = np.array(demodulated_time)

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(time_array, np.real(signal_array), color="b", label="Signal")
        plt.errorbar(
            x=demodulated_time,
            y=np.real(demodulated_signal),
            yerr=np.real(demodulated_signal_error),
            linestyle="none",
            marker="o",
            color="r",
            label="Demodulated Signal",
        )
        plt.xlabel("Time")
        plt.ylabel("Signal")
        plt.title("Dumb Signal demodulation")
        plt.legend()
        plt.show()

    return demodulated_signal, demodulated_time, demodulated_signal_error


def within_threshold(value: float, target_value: float, threshold: float) -> bool:
    """
    Function to check if a value is within a +/- threshold of a given target value.

    :param value: Value to check if it is within the threshold.
    :param target_value: Value to compare the value to.
    :param threshold: +/- threshold
    :return:
    """

    if target_value - threshold < value < target_value + threshold:
        return True
    if value < target_value - threshold or value > target_value + threshold:
        return False


def find_plateaus(
    x_array: np.ndarray, y_array: np.ndarray, threshold: float, plot_graph: bool
) -> np.ndarray:
    """
    Function to search for plateaus in the y array using the second derivative. Returns array of indices, such that
    plateau_indices[::2] gives all the indices at the start of each plateau and plateau_indices[1::2] gives the ends.

    :param x_array: Array for which the array with plateaus is plotted against.
    :param y_array: Array to search for plateaus in.
    :param threshold: value to use as the threshold for which the gradient is non_zero.
    :param plot_graph: Boolean to plot a graph of the y vs x with an overlay of the plateaus
    :return:
    """

    plateau_indices = []
    if within_threshold(float(np.gradient(y_array)[0]), 0, threshold):
        in_plateau = True
        plateau_indices.append(0)
    else:
        in_plateau = False

    for count, gradient in enumerate(np.gradient(y_array)):
        if within_threshold(gradient, 0, threshold) and not in_plateau:
            plateau_indices.append(count)
            in_plateau = True
        if within_threshold(gradient, 0, threshold) and in_plateau:
            continue
        if not within_threshold(gradient, 0, threshold) and in_plateau:
            plateau_indices.append(count)
            in_plateau = False
        if not within_threshold(gradient, 0, threshold) and not in_plateau:
            continue

    # If still in plateau at end of loop, add final index to put an ending bound to last plateau
    if in_plateau:
        plateau_indices.append(y_array.size - 1)

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(x_array, y_array, label="Original Data", color="b", linestyle="--")
        plt.plot(
            x_array[plateau_indices],
            y_array[plateau_indices],
            color="r",
            label="Plateaus",
            linestyle="none",
            marker="o",
        )
        plt.xlabel("x_array")
        plt.ylabel("y_array")
        plt.show()

    return np.array(plateau_indices)


def within_plateau_time_bounds(value: float, time_bound_array: np.ndarray) -> bool:
    """
    Function to check if a given value is within the certain time bounds provided by the time_bound_array. This must be
    of the format such that the slice [::2] gives the start of the various time bounds and [1::2] gives the
    corresponding bound ends

    :param value: value to check
    :param time_bound_array: Array of time bounds of the format such that the slice [::2] gives the start of the
    various time bounds and [1::2] gives the corresponding bound ends.
    :return:
    """

    for index in range(time_bound_array[::2].size - 1):
        # If value is found within a time bound return true
        if time_bound_array[::2][index] < value < time_bound_array[1::2][index]:
            return True
        else:
            continue

    # If not found within any time bounds return false.
    return False


def get_cut_indices(array_to_cut:np.ndarray, bounds_array: np.ndarray) -> np.ndarray:
    """
    Function to find the indices of a given array that correspond to values within the bounding array. The bounds are
    defined in an array of the format such that the slice [::2] gives the start of the various bounds and [1::2] gives
    the corresponding bound ends.

    :param array_to_cut: Array to remove the values not found within the given bounds.
    :param bounds_array: Array of time bounds of the format such that the slice [::2] gives the start of the
    various time bounds and [1::2] gives the corresponding bound ends.
    :return:
    """

    good_indices = np.array([])
    for count, bound_start in enumerate(bounds_array[::2]):
        loop_indices = np.where((bound_start < array_to_cut) & (array_to_cut < bounds_array[1::2][count]))[0]
        good_indices = np.concatenate((good_indices, loop_indices))

    return good_indices.astype(int)
