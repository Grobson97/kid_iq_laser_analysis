import numpy as np
import matplotlib.pyplot as plt
from iq_analysis.util import tools

tmp = np.load("sptslim_sweep.npy", allow_pickle=True, encoding="bytes")
data = tmp.item()
# noinspection PyUnresolvedReferences
keys = list(data.keys())

kid_list = [
    b"K000",
    b"K001",
    b"K002",
    b"K003",
    b"K004",
    b"K005",
    b"K006",
    b"K007",
    b"K008",
    b"K009",
    b"K010",
    b"K011",
    b"K012",
    b"K013",
]

poly_fit_dict = {}  # Dictionary to store all the polynomial fit data for each kid.

for kid_index, kid in enumerate(kid_list):
    # noinspection PyUnresolvedReferences
    frequency_array = data[keys[0]][kid_index] + data[keys[1]]
    # noinspection PyUnresolvedReferences
    iq_array = data[kid_list[kid_index]]
    i_array = np.real(iq_array)
    q_array = np.imag(iq_array)

    tools.plot_iq(
        iq_array, save_plot=False, plot_title=str(kid).replace("b", ""), save_name=""
    )

    # ********** Section to remove cable delay ********** #

    resonance_index = tools.find_resonance_index(i_array, q_array)
    off_resonance_index_array = tools.find_off_resonance_index_array(
        i_array=i_array, q_array=q_array, threshold=0.9
    )

    # noinspection PyUnresolvedReferences
    cable_delay_amplitude, cable_delay_time = tools.fit_cable_delay(
        frequency_array=frequency_array[off_resonance_index_array],
        complex_data_array=data[kid_list[kid_index]][off_resonance_index_array],
        amplitude_guess=np.sqrt(i_array[0] ** 2 + q_array[0] ** 2),
        tau_guess=80e-9,
        plot_graph=False,
    )

    # noinspection PyUnresolvedReferences
    iq_array = data[kid_list[kid_index]] / tools.cable_delay(
        frequency_array, cable_delay_amplitude, cable_delay_time
    )
    i_array = np.real(iq_array)
    q_array = np.imag(iq_array)

    # ********** Section to fit circle ********** #

    x_centre, y_centre, r, residual = tools.circle_fit(i_array, q_array)

    # plt.figure(figsize=(8, 6))
    # tools.plot_circle(x_centre, y_centre, r, label="Circle Fit")
    # plt.plot(i_array, q_array, linestyle="--", marker="o", markersize="4")
    # plt.axis("equal")
    # plt.legend()
    # plt.show()

    # centre and normalise circle:
    i_array = (i_array - x_centre) / r
    q_array = (q_array - y_centre) / r

    # ********** Section to rotate the IQ loop so phase vs frequency is continuous ********** #

    resonance_angle = np.angle(
        i_array[resonance_index] + 1j * q_array[resonance_index], deg=True
    )

    resonance_angle = resonance_angle

    rotated_iq_array = tools.rotate_2d_matrix(
        np.array([i_array, q_array]), resonance_angle
    )
    i_array_rotated = rotated_iq_array[0, :]
    q_array_rotated = rotated_iq_array[1, :]
    phase_array = np.angle(i_array_rotated + 1j * q_array_rotated, deg=True)

    while tools.discontinuity_presence(phase_array, threshold=125):
        rotated_iq_array = tools.rotate_2d_matrix(
            np.array([i_array_rotated, q_array_rotated]), 0.1
        )
        i_array_rotated = rotated_iq_array[0, :]
        q_array_rotated = rotated_iq_array[1, :]
        phase_array = np.angle(i_array_rotated + 1j * q_array_rotated, deg=True)

    tools.plot_iq(
        i_array_rotated + 1j * q_array_rotated,
        save_plot=False,
        plot_title=str(kid).replace("b", "")
        + "Normalised, centred and rotated IQ plot",
        save_name="",
    )

    # plt.figure(figsize=(8, 6))
    # plt.plot(
    #     frequency_array * 1e-9,
    #     phase_array,
    # )
    # plt.xlabel("Frequency (GHz)")
    # plt.ylabel("Phase Angle (degrees)")
    # plt.show()

    # ********** Section to fit polynomial to extract frequency for a given phase ********** #

    quarter = int(frequency_array.size / 4)
    polynomial_fit = tools.fit_polynomial(
        phase_array[resonance_index - quarter : resonance_index + quarter],
        frequency_array[resonance_index - quarter : resonance_index + quarter] * 1e-9,
        degree=6,
        plot_graph=True,
    )
    print(str(kid).replace("b", "") + "fitting complete")

    poly_fit_dict[str(kid).replace("b", "")] = polynomial_fit

print(poly_fit_dict)
